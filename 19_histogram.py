from random import randint
import random
import threading 

def histogram(tab):
    tab_range=max(tab)+1
    b=[x for x in range(tab_range)]
    a=[0 for i in range(tab_range)]
    for el in range(1,len(tab)):
        a[tab[el]]+=1
    dictionary = dict(zip(b, a))
    print(dictionary)
    return dictionary


num_of_elements=int(input("Please type number of elements: "))
tab_range=int(input("Please type range of numbers: "))
tab=[ random.randint(0, tab_range) for i in range(num_of_elements)]

a=tab[0:(int(num_of_elements/4))]
b=tab[((int(num_of_elements/4))+1):(int(2*num_of_elements/4))]
c=tab[((int(2*num_of_elements/4))+1):(int(3*num_of_elements/4))]
d=tab[((int(3*num_of_elements/4))+1):(int(num_of_elements))]

final=[a,b,c,d]
threads=[]
for i in final:
    t= threading.Thread(target=histogram, args=(i,))
    threads.append(t)
    t.start()
