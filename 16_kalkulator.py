class Complex(object):
    def __init__(self, real, imag=0.0): #by default imag=0
        self.real = real
        self.imag = imag

    def __add__(self, other):
        return Complex(self.real + other.real,
                       self.imag + other.imag)

    def __sub__(self, other):
        return Complex(self.real - other.real,
                       self.imag - other.imag)
    def __mul__(self, other):
        return Complex(self.real*other.real - self.imag*other.imag,
                       self.imag*other.real + self.real*other.imag)

    def __div__(self, other):
#         sr, si, or, oi = self.real, self.imag, \ 
#                          other.real, other.imag # short forms
        r = float(other.real**2 + other.imag**2)
        return Complex((self.real*other.real+self.imag*other.imag)/r, (self.imag*other.real-self.real*other.imag)/r)
    def __str__(self):
        return '(%g, %g)' % (self.real, self.imag)

a=int(input("Enter real part of first number : "))
b=int(input("Enter imaginary part of first number: "))
c=int(input("Enter real part of second number : "))
d=int(input("Enter imaginary part of second number: "))
obj1=Complex(a,b)
obj2=Complex(c,d)

choice=1
while choice!=0:
    print("0. Exit")
    print("1. Add")
    print("2. Subtraction")
    print("3. Multiplication")
    print("4. Division")
    choice=int(input("Enter choice: "))
    if choice==1:
        print("Result: ",obj1.__add__(obj2))
    elif choice==2:
        print("Result: ",obj1.__sub__(obj2))
    elif choice==3:
        print("Result: ",obj1.__mul__(obj2))
    elif choice==4:
        print("Result: ",obj1.__div__(obj2))
    elif choice==0:
        print("Exiting!")
    else:
        print("Invalid choice!!")
 