import json

def read(file):
    with open('sample.json') as data_file:
        data = json.load(data_file)
        print(data)
        
def write_record(file, review):
    with open('sample.json', 'a+') as outfile:
        json.dump(review, outfile)

def delete_record(file,title):
    with open('sample.json') as data_file:
        data = json.load(data_file)

    for element in data:
        if title in element:
            del element['title']
            

    with open('sample.json', 'w') as data_file:
        data = json.dump(data, data_file)   
        

file="sample.json"
a=1
while a!=0:
    print("0. Exit")
    print("1. Read current reviews")
    print("2. Add new review")
    print("3. Delete review")
    a=int(input("podaj liczbe: "))    
    if a == 1:
        read(file)
    elif a == 2:
        record=''
        title=input("Please type title of movie: ")
        review=input("Please type your review: ")
        row_to_insert= {"title":title,"review":review}
        write_record(file,row_to_insert)
    elif a == 3:
        title=input("Please type title you want to delete: ")
        delete_record(file,title)
    elif a == 0:
        print("Exiting!")