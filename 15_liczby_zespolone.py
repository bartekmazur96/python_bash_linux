class Complex(object):
    def __init__(self, real, imag=0.0): #by default imag=0
        self.real = real
        self.imag = imag

    def __add__(self, other):
        return Complex(self.real + other.real,
                       self.imag + other.imag)

    def __sub__(self, other):
        return Complex(self.real - other.real,
                       self.imag - other.imag)
    def __str__(self):
        return '(%g, %g)' % (self.real, self.imag)
a = Complex(2,-1) 
b = Complex(1,4)
print(a-b)