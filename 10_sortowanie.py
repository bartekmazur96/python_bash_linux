from time import clock
from random import randint
import random

def bsort(tab):
    start = clock()
    for i in range (1,len(tab)):
        for j in range (0, len(tab)-i):
            if tab[j]>tab[j+1]:
                tab[j], tab[j+1] = tab[j+1], tab[j]
    stop = clock() - start
    print("Time of BubbleSort:  " + str(stop))

def sort(tab):
    start = clock()
    tab.sort()
    stop = clock() - start
    print("Time of built-in sort method:  " + str(stop))
    
tab2=[]
tab2 = random.sample(range(0,1000), 50)
bsort(tab2.copy())
#something
sort(tab2.copy())
